//  openforum, a blockchain based forum software
//  Copyright (C) 2016  Waylon Cude
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

//TODO: custom NetworkListener
#![feature(proc_macro)]
#![feature(slice_patterns)]

#[macro_use]
extern crate serde_derive;

extern crate serde;
extern crate serde_json;
extern crate hyper;
extern crate chrono;
extern crate crypto;
extern crate url;
extern crate merkle;
extern crate toml;
extern crate getopts;
extern crate openssl;

mod config;
mod handler;
mod action;
mod peer;
mod block;
mod util;

use std::env;
use std::path::PathBuf;

use config::Config;
use config::DEFAULT_CONFIG_DIR;

use block::Block;

use hyper::server::Server;
use hyper::net::Openssl;

use getopts::Options;

fn main() {
    let args: Vec<String> = env::args().collect();
    let program = args[0].clone();
    let mut opts = Options::new();
    opts.optflag("h", "help", "print this help menu");
    //TODO: set config file instead of directory
    //Maybe error out if config doesn't exist at specified location
    opts.optopt("c","config", "sets the config file","FILE");
    let matches = match opts.parse(&args[1..]) {
        Ok(m) => { m }
        Err(f) => { panic!(f.to_string()) }
    };
    if matches.opt_present("h") {
        print_usage(&program, opts);
        return;
    }
    let config_path = match matches.opts_str(&["c".to_owned(),"config".to_owned()]) {
        //I probably don't want to create a config file 
        //at the specified path
        Some(path) => PathBuf::from(path),
        None => {
            let mut config_dir = env::home_dir().expect("No home folder");
            config_dir.push(DEFAULT_CONFIG_DIR);
            config::create_if_missing(config_dir.clone());
            config_dir.push("config.toml");
            config_dir
        }
        };
    println!("Using config file {:?}",config_path);
    let config = Config::from_file(config_path).expect("Problem parsing config file");
    //Ask config what the newest locally stored block is
    let mut block_number = config.get_block_number();
    let handler = handler::Handler::new(config.clone(),block_number);
    //Ask peers for the newest block
    for p in config.peers() {
        match p.get_current_block_number() {
            Ok(num) => if num > block_number {
                //switch_chains(x) only downloads blocks up to x-1 
                match handler.switch_chains(num+1) { 
                    Ok(_) => {
                        block_number = num;
                    },
                    Err(e) => println!("Error switching chain: {:?}",e)
                }
            },
            Err(e) => println!("Error connecting to peer: {:?}",e)
        }
    }
    //If no blocks exist then create genesis block
    if block_number == 0 {
        Block::to_file(&Block::genesis_block(),&config).expect("Problem creating genesis block");
        println!("Genesis block created");
    }
    let ssl = match config.ssl() {
        Ok(ssl) => ssl,
        Err(e) => {
            println!("Error starting server: {}",e);
            println!("Have you specified a key and cert in your config file?");
        }
    };
    println!("Starting server");
    Server::https("0.0.0.0:32137",ssl)
        .unwrap()
        .handle(handler)
        .unwrap();
}
fn print_usage(program: &str, opts: Options) {
    let brief = format!("Usage: {} [options]", program);
    print!("{}", opts.usage(&brief));
}
