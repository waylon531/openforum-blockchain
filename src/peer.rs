//  openforum, a blockchain based forum software
//  Copyright (C) 2016  Waylon Cude
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

//TODO: return errors based on status code
//
//TODO: Use openssl library to verify certificate before sending it
//TODO: Encrypt data with key
//Do I need to write a new network connector to add the extra encryption?
//Should I just use TCP and serialization?
//Probably
//Hyper is probably needless overhead
//What about easy access from the web?
//
use std::net;

use hyper;
use hyper::client::Client;
use hyper::header::{Headers, Authorization};
use hyper::status::StatusCode;

use url;

use serde_json;

use action::Action;
use util;
use block::Block;

#[derive(Debug)]
pub enum PeerError {
    JSONError(serde_json::error::Error),
    HttpError(hyper::error::Error),
    InvalidAddr(url::ParseError),
    BadStatusCode(StatusCode),
	InvalidAuth
}
#[derive(Debug,Clone,Serialize,Deserialize)]
pub struct Peer {
    key: Option<String>,
    //Problem: SocketAddr doesn't take URLs
    //TODO: take url
    address: net::SocketAddr,
    fingerprint: String //String of hex characters
}
impl From<hyper::error::Error> for PeerError {
    fn from(e: hyper::error::Error) -> PeerError {
        PeerError::HttpError(e)
    }
}

impl From<serde_json::error::Error> for PeerError {
    fn from(e: serde_json::error::Error) -> PeerError {
        PeerError::JSONError(e)
    }
}

impl Peer {
    pub fn submit_action(&self, action: Action) -> Result<(), PeerError> {
        let client = Client::new();
        let json_string = try!(serde_json::to_string(&action));
        let mut headers = Headers::new();
        headers.set(Authorization(match self.key{Some(ref n) => n,None => return Err(PeerError::InvalidAuth)}.clone()));
		let mut url = match util::urlify(self.address) {
                Ok(url) => url,
                Err(e) => return Err(PeerError::InvalidAddr(e)),
            };
		url.set_path("action");
        let response_result = client.post(url)
            .headers(headers)
            .body(&json_string)
            .send();
        match response_result {
            Ok(res) => {
                if res.status == StatusCode::Ok {
                    Ok(())
                } else {
                    Err(PeerError::BadStatusCode(res.status))
                }
            },
            Err(e) => Err(PeerError::HttpError(e)),
        }
    }
    pub fn submit_block(&self, block: Block) -> Result<(), PeerError> {
        let client = Client::new();
        let json_string = try!(serde_json::to_string(&block));
        let mut headers = Headers::new();
        headers.set(Authorization(match self.key{Some(ref n) => n,None => return Err(PeerError::InvalidAuth)}.clone()));
		let mut url = match util::urlify(self.address) {
                Ok(url) => url,
                Err(e) => return Err(PeerError::InvalidAddr(e)),
            };
		url.set_path("block");
        let response_result = client.post(url)
            .headers(headers)
            .body(&json_string)
            .send();
        match response_result {
            Ok(res) => {
                if res.status == StatusCode::Ok {
                    Ok(())
                } else {
                    Err(PeerError::BadStatusCode(res.status))
                }
            },
            Err(e) => Err(PeerError::HttpError(e)),
        }
    }
    pub fn get_block(&self, block_number: u64) -> Result<Block,PeerError> {
        let client = Client::new();
        let headers = Headers::new();
		let mut url = match util::urlify(self.address) {
                Ok(url) => url,
                Err(e) => return Err(PeerError::InvalidAddr(e)),
            };
		url.set_path(("block".to_owned() + block_number.to_string().as_str()).as_str());
        let response_result = client.post(url)
            .headers(headers)
            .body("")
            .send();
        match response_result {
            Ok(res) => {
                if res.status == StatusCode::Ok {
                    Ok(try!(serde_json::from_reader(res)))
                } else {
                    Err(PeerError::BadStatusCode(res.status))
                }
            },
            Err(e) => Err(PeerError::HttpError(e)),
        }
    }
    pub fn get_current_block_number(&self) -> Result<u64,PeerError> {
        let client = Client::new();
        let headers = Headers::new();
		let mut url = match util::urlify(self.address) {
                Ok(url) => url,
                Err(e) => return Err(PeerError::InvalidAddr(e)),
            };
		url.set_path("currentblock");
        let response_result = client.post(url)
            .headers(headers)
            .body("")
            .send();
        match response_result {
            Ok(res) => {
                if res.status == StatusCode::Ok {
                    Ok(try!(serde_json::from_reader(res)))
                } else {
                    Err(PeerError::BadStatusCode(res.status))
                }
            },
            Err(e) => Err(PeerError::HttpError(e)),
        }
    }
}
