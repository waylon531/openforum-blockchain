//  openforum, a blockchain based forum software
//  Copyright (C) 2016  Waylon Cude
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

#[derive(Serialize, Deserialize,Clone,Debug,PartialEq)]
pub enum Action {
    // Post diff
    Post(Diff, Signature),
    // Post your public key and sign it (eliminates need for keyservs)
    PostPublicKey(PublicKey, Signature),
    // Give a user a name
    ID(PublicKey, Channel, IDMessage, Signature),
    // Trust other users to contribute
    Trust(PublicKey, TrustLevel, TrustType, Vec<TrustOption>),
    // Vote on an action (channels allow for up/down and others)
    Vote(ActionNumber, Channel, Signature),
}
#[derive(Serialize, Deserialize,Clone,Debug,PartialEq)]
pub struct Diff {
    body: String,
}
#[derive(Serialize, Deserialize,Clone,Debug,PartialEq)]
pub struct PublicKey {
    key: String,
}
#[derive(Serialize, Deserialize,Clone,Debug,PartialEq)]
pub struct Signature {
    sig: String,
}
#[derive(Serialize, Deserialize,Clone,Debug,PartialEq)]
pub struct Channel {
    channel: u64,
}
#[derive(Serialize, Deserialize,Clone,Debug,PartialEq)]
pub struct IDMessage {
    message: String,
}
#[derive(Serialize, Deserialize,Clone,Debug,PartialEq)]
pub struct ActionNumber {
    block_number: u64,
    action_number: u64,
}
#[derive(Serialize, Deserialize,Clone,Debug,PartialEq)]
pub enum TrustOption {
    NoPropogate, // Only trusted for yourself
    Leaf, // User can't trust others
    RestrictTopic(String), // Limit trust to a specific topic
}
#[derive(Serialize, Deserialize,Clone,Debug,PartialEq)]
pub enum TrustType {
    // What the user is given permission to do
    Post,
    ID,
    Vote,
}
#[derive(Serialize, Deserialize,Clone,Debug,PartialEq)]
pub struct TrustLevel {
    level: i64,
}
