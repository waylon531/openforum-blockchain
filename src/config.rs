//  openforum, a blockchain based forum software
//  Copyright (C) 2016  Waylon Cude
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

use std::option::Option;
use std::io;
use std::io::{Error,ErrorKind, Write,Read};
use std::fs;
use std::fs::File;
use std::path;
use std::slice;

use openssl::error::SslError;
use hyper::net::Openssl;
use hyper::header::Authorization;

use peer::Peer;

use toml;

pub const DEFAULT_CONFIG_DIR: &'static str = ".openforum";

// Config is not mutated during execution, it is safe to enclose it in a static
#[derive(Debug,Clone,Serialize,Deserialize)]
pub struct Config {
    keys: Vec<String>,
    peers: Vec<Peer>,
    block_dir: path::PathBuf,
    cert: path::PathBuf,
    key: path::PathBuf,
}
impl Config {
    //Unused function
    //pub fn new(block_dir: path::PathBuf) -> Config {
    //    Config {
    //        keys: vec![],
    //        peers: vec![],
    //        block_dir: block_dir,
    //    }
    //}
    pub fn peers(&self) -> slice::Iter<Peer> {
        self.peers.iter()
    }
    pub fn from_file(file_name: path::PathBuf) -> io::Result<Config> {
        let mut f = try!(File::open(file_name));
        let mut buffer = String::new();
        try!(f.read_to_string(&mut buffer));
        match toml::decode_str(buffer.as_str()) {
            Some(c) => Ok(c),
            None => Err(Error::new(ErrorKind::InvalidData,"Error parsing toml"))
        }
    }
    pub fn get_block_number(&self) -> u64 {
        let mut block_number = 0;
        for e in fs::read_dir(&self.block_dir).expect("Bad block directory") {
            match e {
                Ok(entry) => match entry.file_name().into_string() {
                    Ok(s) => {
                        let mut split_iter = s.as_str().split('.');
                        let number: u64 = match split_iter.next() {
                            None => break,
                            Some(s) => match s.parse() {
                                Ok(num) => num,
                                Err(_) => break
                            }
                        };
                        if number > block_number {
                            block_number = number;
                        }
                    },
                    Err(_) => {}
                },
                Err(err) => println!("Bad file: {:?}",err)
            }
        }
        block_number
    }
    pub fn validate_password(&self, password: Option<&Authorization<String>>) -> bool {
        match password {
            None => false, //Invalid custom_headers
            Some(ref pass) => self.keys.contains(&pass),
        }
    }
    pub fn get_block_dir(&self) -> &path::PathBuf {
        &self.block_dir
    }
    pub fn ssl(&self) -> Result<Openssl,SslError> {
        Openssl::with_cert_and_key(&self.cert,&self.key)
    }
}
pub fn create_if_missing(config_dir: path::PathBuf) {
    match fs::read_dir(config_dir.clone()) {
        Err(e) => {
            match e.kind() {
                ErrorKind::NotFound => {
                    fs::create_dir(config_dir.clone()).expect("Failed to create config directory");
                    println!("Config directory created");
                    create_config(config_dir);
                }
                _ => {
                    panic!("Config directory read error: {}", e);
                }
            }
        }
        Ok(mut files) => {
            if !files.any(|x| &x.unwrap().file_name() == "config.toml") {
                // If config file doesn't exist then create it
                create_config(config_dir);
                println!("Config file created");
            }
        }
    }
}
fn create_config(mut config_dir: path::PathBuf) {
    // panics are okay here because if this function isn't working
    // then the whole program is broken / missing config
    let mut block_dir = config_dir.clone();
    block_dir.push("blocks");
    fs::create_dir(block_dir.clone()).expect("Failed to create block directory");
    config_dir.push("config.toml");
    match fs::File::create(config_dir) {
        Err(e) => panic!("Failed to create config file: {}", e),
        Ok(mut f) => {
            match write!(f,
r#"block_dir = "{}"
cert = ""
key = ""
#keys = ["a_key","another_key"]
keys  = []

#[[peers]]
#key = "b"
#address= "0.0.0.0:80"

#[[peers]]
#key = "c"
#address= "0.0.0.0:81"
"#,
            block_dir.to_str().expect("Invalid block path")) {
                Err(e) => panic!("Failed writing to config file: {}", e),
                Ok(_) => println!("Successfully created config file"),
            }
        }
    }
}
