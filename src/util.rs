//  openforum, a blockchain based forum software
//  Copyright (C) 2016  Waylon Cude
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

use std::net;
use std::io;
use url::ParseError;
use hyper::Url;
use hyper::uri::RequestUri;
use serde_json;
use handler::HandlerError;
//Parses a uri into the segments separated by slashes
//Returns a str so I can pattern match it
pub fn parse_uri<'a>(uri: &'a RequestUri) -> Result<Vec<&'a str>,HandlerError> {
    match uri {
        &RequestUri::AbsolutePath(ref u) => Ok(u.split('/').collect()),
        &RequestUri::AbsoluteUri(ref u) => 
            match u.path_segments() {
                Some(path) => Ok(path.collect()),
                None => return Err(HandlerError::InvalidUri),
            },
        _ => Err(HandlerError::InvalidUri)
    }
}
// Creates a floating point number from an array of bytes
pub fn sixty_four_bytes_to_f64(bytes: [u8; 64]) -> f64 {
    let mut result = 0f64;
    for n in 0..63 {
        // let n = 63 - i; //Preserve endianness
        result += bytes[n] as f64; //Add byte to result
        result *= 8f64.exp2(); //Shift left by a byte
    }
    result += bytes[63] as f64; //Don't shift this byte
    result
}
// Generates a URL from an ip address
pub fn urlify(addr: net::SocketAddr) -> Result<Url, ParseError> {
    match addr {
        net::SocketAddr::V4(_) => {
            Url::parse(&(
                    //These pluses look pretty ugly at the start of the line
                    "https://".to_owned()
                    + &addr.ip().to_string()
                    + ":"
                    + &addr.port().to_string()
                ))
        }
        net::SocketAddr::V6(_) => {
            Url::parse(&("https://[".to_owned() + &addr.ip().to_string() + "]:" +
                         &addr.port().to_string()))
        }
    }
}
// Converts a serde error into an io error
pub fn serde_error_to_io_error(error: serde_json::error::Error) -> io::Error {
    match error {
        serde_json::error::Error::Io(e_io) => return e_io,
        // Syntax error, return a io error
        serde_json::error::Error::Syntax(_, _, _) => {
            return io::Error::new(io::ErrorKind::InvalidData, error)
        }
    }

}
#[test]
fn bytes_16777215_to_f64() {
    assert_eq!(sixty_four_bytes_to_f64([0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                                        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                                        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                                        0, 0, 0, 0, 255, 255, 255]),
               16777215f64);
}
#[test]
fn bytes_65535_to_f64() {
    assert_eq!(sixty_four_bytes_to_f64([0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                                        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                                        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                                        0, 0, 0, 0, 0, 255, 255]),
               65535f64);
}
#[test]
fn bytes_0_to_f64() {
    assert_eq!(sixty_four_bytes_to_f64([0; 64]), 0f64);
}
#[cfg(test)]
use std::net::{Ipv4Addr, SocketAddr, Ipv6Addr, IpAddr};
#[test]
fn urlify_test() {
    urlify(SocketAddr::new(IpAddr::V4(Ipv4Addr::new(42, 42, 42, 42)), 12345)).unwrap();
}
#[test]
fn urlify_ipv6_test() {
    urlify(SocketAddr::new(IpAddr::V6(Ipv6Addr::new(42, 42, 42, 42, 42, 42, 42, 42)),
                           12345))
        .unwrap();
}
#[test]
fn absolute_uri_parse() {
    let mut url = urlify(SocketAddr::new(IpAddr::V4(Ipv4Addr::new(42, 42, 42, 42)), 12345)).unwrap();
    url.set_path("this/is/a/test");
    assert_eq!(vec!["this","is","a","test"],
               parse_uri(&RequestUri::AbsoluteUri(url)).unwrap());
}
#[test]
fn absolute_path_parse() {
    assert_eq!(vec!["this","is","another","test"],
               parse_uri(&RequestUri::AbsolutePath("this/is/another/test".to_owned())).unwrap());
}
