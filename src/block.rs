//  openforum, a blockchain based forum software
//  Copyright (C) 2016  Waylon Cude
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

use std::io;
use std::io::Write;
use std::fs::File;

use chrono::datetime;
use chrono::offset::utc;

use crypto::sha3;
use crypto::digest::Digest;

use merkle::MerkleTree;

use serde_json;

use util;
use action;
use config::Config;
#[derive(Debug)]
pub enum BlockError {
    InvalidBlockNumber,
    InvalidTimestamp,
    BlockFromFuture,
    NotDifficultEnough(f64,f64), //Hardness of block, target
    JSONError(serde_json::error::Error),
    BadMerkleHash,
    SwitchChain(Block), //Contains the already downloaded block
                        //which has the number contained within
}
impl From<serde_json::error::Error> for BlockError {
    fn from(e: serde_json::error::Error) -> BlockError {
        BlockError::JSONError(e)
    }
}

#[derive(Serialize,Deserialize,Clone,Debug)]
pub struct Block {
    previous_hash: String,
    merkle_hash: String,
    time: datetime::DateTime<utc::UTC>,
    nonce: u64,
    difficulty: f64, //PROBLEM: f64s will truncate out small numbers,
                     //which makes difficulty not exactly what it should be
    actions: Vec<action::Action>, /* Vec is okay here because actions are mainly
                                   * just pointers to strings */
    block_number: u64, //So when these are serialized the reciever
                       //knows which block this is
}
impl Block {
    pub fn create_block(time: datetime::DateTime<utc::UTC>,block_number: u64) -> Block {
        let null_vec: Vec<String> = vec![];
        Block {
            previous_hash: "".to_owned(),
            merkle_hash: MerkleTree::new(null_vec).get_root_hash(sha3::Sha3::sha3_512()),
            time: time,
            nonce: 0,
            difficulty: 512f64.exp2(), //All blocks fit this difficulty
            actions: vec![],
            block_number: block_number,
        }
    }
    pub fn genesis_block() -> Block {
        Block::create_block(utc::UTC::now(),0) //It's the current time!
    }
    pub fn verify(&self, previous_block: &Block) -> Result<(), BlockError> {
        // check if the number of the current block is greater than prev
        // If there's more than 1 difference then switch chains
        
        if self.block_number < previous_block.block_number {
            return Err(BlockError::InvalidBlockNumber);
        } else if self.block_number > previous_block.block_number + 1 {
            //Switch chains in place
            //So then we can continue with verifying the block
            //Otherwise we have to return and then call this function again
            //That might actually be better, once I've downloaded missing
            //blocks this function should run normally
            //
            //Clone is still faster than redownloading it
            return Err(BlockError::SwitchChain(self.clone()));
        }
       
        // If a block is newer than the current block then ask the server
        // that sent it for the blockchain and temporarily stop recieving
        // new blocks
        // I probably need a block number field for this to work
        //
        // Make sure this block has a timestamp greater than that
        // of the previous block
        if self.time.timestamp() <= previous_block.time.timestamp() {
            return Err(BlockError::InvalidTimestamp);
        } else if self.time.timestamp() > utc::UTC::now().timestamp() {
            return Err(BlockError::BlockFromFuture);
        }
        let time_diff = (self.time.timestamp() - previous_block.time.timestamp()) as f64;
        // Calculate difficulty target
        // PROBLEM: ridiculous amounts of orphans will be created because blocks
        // are very large and take lots of time to distribute out over the network
        // Solution?: Very slow blocks, propogation time < creation time
        //
        // PROBLEM: Blocks are allowed to set the difficulty
        // and calculating the difficulty from the stamps in each of the blocks
        // is fairly expensive
        let difficulty = previous_block.get_difficulty()
            //decrease initial difficulty, allowing difficulty to grow
            / 5f64.exp2()
            //Double difficulty every minute, average block finding time is 5 minutes
            * (time_diff).exp2();
        let mut buffer = [0u8; 64];
        // Use Sha3_512
        let mut hasher = sha3::Sha3::sha3_512();
        //Hash JSON representation of block
        hasher.input_str(&try!(serde_json::to_string(self)));
        hasher.result(&mut buffer);
        let hash = util::sixty_four_bytes_to_f64(buffer);
        // The hash needs to be smaller than the difficulty to be accepted
        // All hashes are accepted a 2^512 difficulty, so it is the max
        // difficulty
        if hash < difficulty.min(512f64.exp2()) {
            let mut json_actions = vec![];
            //I can't use map here if I want to directly return an error 
            for ref e in &self.actions {
                json_actions.push(try!(serde_json::to_string(e)));
            }
                    
            //And check that the merkle root is valid
            if MerkleTree::new(json_actions)
                .get_root_hash(sha3::Sha3::sha3_512())
                == self.merkle_hash 
            {
                return Ok(());
            } else {
                return Err(BlockError::BadMerkleHash);
            }
        } else {
            return Err(BlockError::NotDifficultEnough(hash,difficulty));
        }
    }
    fn get_difficulty(&self) -> f64 {
        // This can be implemented by looking at the timestamps of all
        // previous blocks
        // It'd be really slow though
        self.difficulty
    }
    pub fn get_actions(&self) -> &Vec<action::Action> {
        &self.actions
    }
    pub fn get_number(&self) -> u64 {
        self.block_number
    }
    pub fn to_file(&self, config: &Config) -> Result<(), io::Error> {
        let mut file_to_write = config.get_block_dir().clone();
        file_to_write.push(self.get_number().to_string()+".block");
        let mut file = try!(File::create(file_to_write));
        let serialized_block: String = match serde_json::to_string(&self) {
            Ok(s) => s,
            Err(e) => return Err(util::serde_error_to_io_error(e)),
        };
        try!(file.write_all(&serialized_block.into_bytes()));
        Ok(())
    }
    pub fn from_file(num: u64, config: &Config) -> Result<Block, io::Error> {
        let mut file_to_read = config.get_block_dir().clone();
        file_to_read.push(num.to_string()+".block");
        let file = try!(File::open(file_to_read));
        match serde_json::from_reader(file) {
            Ok(b) => Ok(b),
            Err(e) => Err(util::serde_error_to_io_error(e)),
        }
    }
    pub fn get_file(num: u64, config: &Config) -> Result<File, io::Error> {
        let mut file_to_write = config.get_block_dir().clone();
        file_to_write.push(num.to_string());
        file_to_write.push(".block");
        Ok(try!(File::open(file_to_write)))
    }
}
#[cfg(test)]
mod test {
use block::Block;
use chrono;
use chrono::offset::utc;
    #[test]
    fn genesis_block_verification_test() {
        let past_block = Block::create_block(utc::UTC::now().checked_sub(chrono::Duration::minutes(5)).unwrap(),0);
        Block::create_block(utc::UTC::now(),1).verify(&past_block).unwrap();
    }
    //TODO: test error conditions
}
