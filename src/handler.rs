//  openforum, a blockchain based forum software
//  Copyright (C) 2016  Waylon Cude
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

use std::io;
use std::io::{Read, Write};
use std::sync;
use std::sync::Mutex;
use std::collections::VecDeque;
use std::num;

use hyper;
use hyper::server;
use hyper::server::{Request, Response};
use hyper::status::StatusCode;
use hyper::header::Authorization;

use serde_json;

use config;
use action::Action;
use block;
use block::{Block,BlockError};
use peer::PeerError;
use util;

#[derive(Debug)]
pub enum HandlerError {
    //Error encoding
    EncoderError(serde_json::error::Error),
    //Error either encoding or decoding
    JSONError(serde_json::error::Error),
    ActionPoisoned,
    //Generic poisoning error
    Poisoned,
    DuplicateAction,
    IOError(io::Error),
    BlockError(block::BlockError),
    WrongNumberOfBlocks(u64,u64), //Number of downloaded blocks, target
    NoGoodPeers,
    PeerError(PeerError),
    InvalidUri,
    ParseIntError(num::ParseIntError)
}
impl From<io::Error> for HandlerError {
    fn from(e: io::Error) -> HandlerError {
        HandlerError::IOError(e)
    }
}

impl From<serde_json::error::Error> for HandlerError {
    fn from(e: serde_json::error::Error) -> HandlerError {
        HandlerError::JSONError(e)
    }
}
impl<T> From<sync::PoisonError<T>> for HandlerError {
    fn from(_: sync::PoisonError<T>) -> HandlerError {
        HandlerError::Poisoned
    }
}
impl From<num::ParseIntError> for HandlerError {
    fn from(e: num::ParseIntError) -> HandlerError {
        HandlerError::ParseIntError(e)
    }
}
impl From<block::BlockError> for HandlerError {
    fn from(e: block::BlockError) -> HandlerError {
        HandlerError::BlockError(e)
    }
}

impl From<PeerError> for HandlerError {
    fn from(e: PeerError) -> HandlerError {
        HandlerError::PeerError(e)
    }
}

pub struct Handler {
    action_queue: Mutex<Vec<Action>>,
    // Mutex instead of atomics to lock when adding block?
    // Otherwise race condition where multiple blocks get validated
    block_number: Mutex<u64>,
    config: config::Config,
}
impl Handler {
    pub fn new(conf: config::Config, block_number: u64) -> Handler {
        Handler {
            config: conf,
            action_queue: Mutex::new(vec![]),
            block_number: Mutex::new(block_number),
        }
    }
    fn get_block<'a,W: Write>(&self, block_num_str: &'a str, res: &mut W) -> Result<(), HandlerError> {
        let block_num = try!(block_num_str.parse());
        try!(io::copy(&mut try!(Block::get_file(block_num, &self.config)), res));
        Ok(())
    }
    fn get_action<'a,W: Write>(&self, block_num_str: &'a str,action_num_str: &'a str, mut res: &mut W) -> Result<(), HandlerError> {
        // Convenience wrapper around get_block
        // So clients don;t have to parse through a whole block
        let block_num: u64 = try!(block_num_str.parse());
        let action_num: u64 = try!(action_num_str.parse());
        try!(serde_json::to_writer(
                &mut res, 
                &try!(
                    Block::from_file(block_num,&self.config))
                    .get_actions()[action_num as usize]
                ));
        Ok(())
    }
    fn get_action_queue<W: Write>(&self,
                                           res: &mut W)
                                           -> Result<(), HandlerError> {
        match self.action_queue.lock() {
            // v is derefenced to lock, then referenced to pass to encoder
            Ok(v) => {
                match serde_json::to_string(&*v) {
                    Ok(s) => Ok(try!(res.write_all(s.as_bytes()))),
                    Err(e) => Err(HandlerError::EncoderError(e)),
                }
            }
            Err(_) => Err(HandlerError::ActionPoisoned),
        }
    }
    fn get_current_block_number<W: Write>(&self,
                                           res: &mut W)
                                           -> Result<(), HandlerError> {
        //Writes the number of the current block into the stream
        Ok(try!(res.write_all(try!(self.block_number.lock()).to_string().as_bytes())))
    }
    fn submit_action<R: Read, W: Write>(&self,
                                        req: &mut R,
                                        res: &mut W)
                                        -> Result<(), HandlerError> {
        let action: Action = match serde_json::from_reader(req) {
            Ok(k) => k,
            Err(e) => return Err(HandlerError::EncoderError(e)),
        };
        let action_cloned = action.clone(); //Clone outside of mutex lock
        {
            // Block to prevent poison and minimize lock time
            match self.action_queue.lock() {
                Ok(mut t) => {
                    if t.contains(&action) {
                        // Don't add duplicate actions to the queue
                        return Err(HandlerError::DuplicateAction);;
                    } else {
                        t.push(action_cloned)
                    }
                }
                Err(_) => return Err(HandlerError::ActionPoisoned),
            }
        }
        // Submit action to peers
        for peer in self.config.peers() {
            //ignore result
            let _ = peer.submit_action(action.clone());
        }
        return Ok(try!(res.write_all(b"Action submitted")));
    }
    // Recieves and verifies a block, then sends it out to peers
    fn submit_block<R: Read, W: Write>(&self,
                                       req: &mut R,
                                       res: &mut W)
                                       -> Result<(), HandlerError> {
        // Blocks have to be verified in order so the block verify function
        // has a previous block to work against
        let block_to_verify: Block = try!(serde_json::from_reader(req));
        self.submit_downloaded_block(block_to_verify,res)
    }
    fn submit_downloaded_block<W: Write>(&self,block_to_verify: Block,res:&mut W) -> Result<(), HandlerError> {
        // block_number is locked for the whole funtion to prevent race conditions
        let mut block_number = try!(self.block_number.lock());
        try!(block_to_verify.verify(
            &try!(Block::from_file(*block_number,&self.config))
            ));
        //try will return early if there is an error
        try!(block_to_verify.to_file(&self.config));
        //Silently continue if client hung up
        let _ = res.write_all(b"Successfully verified block");
        *block_number += 1;
        let mut action_queue = try!(self.action_queue.lock());
        //Retain elements that did not get verified in the block
        action_queue.retain(|action| !block_to_verify.get_actions().contains(action)); 
        //Send block out to all peers
        for peer in self.config.peers() {
            //Throw away result
            let _ = peer.submit_block(block_to_verify.clone());
        }
        Ok(()) 
    }
    pub fn switch_chains(&self,b_num: u64) -> Result<(),HandlerError> {
        //TODO: clean up this function
        
        //Prevent race conditions
        let block_number = try!(self.block_number.lock());
        let mut blocks_to_verify = VecDeque::new();
        let block_number_to_get = b_num;
        //TODO: multi threaded download
        let mut good_peer_option = None;
        for peer in self.config.peers() {
            //Download from first peer that has the wanted block
            let current_block_num = peer.get_current_block_number();
            if current_block_num.is_ok() && try!(current_block_num) >= block_number_to_get {
                good_peer_option = Some(peer);
                break;
            }
        }
        let good_peer = match good_peer_option {
            Some(p) => p,
            None => return Err(HandlerError::NoGoodPeers)
        };
        for b in *block_number + 1 .. block_number_to_get {
            blocks_to_verify.push_back(try!(good_peer.get_block(b)));
        }
        let mut current_block = try!(Block::from_file(*block_number,&self.config));
        if *block_number + blocks_to_verify.len() as u64 == block_number_to_get - 1{
            //Until the first block verifies
            while try!(blocks_to_verify[0].verify(&current_block)) == (){
                let num = current_block.get_number() - 1;
				blocks_to_verify.push_front(try!(good_peer.get_block(num)));
            	current_block = try!(Block::from_file(num,&self.config));
            }
            //Validate all blocks
            for i in 1 .. blocks_to_verify.len() {
                try!(blocks_to_verify[i].verify(&blocks_to_verify[i-1]));
            }
            //then write them to files
            for i in 1 .. blocks_to_verify.len() {
                try!(Block::to_file(&blocks_to_verify[i],&self.config));
            }
            Ok(())
        } else {
            return Err(HandlerError::WrongNumberOfBlocks(*block_number + blocks_to_verify.len() as u64,block_number_to_get));
        }
    }
}
// Possible TODO: set content length
impl server::Handler for Handler {
    fn handle(&self, mut req: Request, mut res: Response) {
        match req.method {
            hyper::Post => {
                if self.config.validate_password(req.headers.get::<Authorization<String>>()) {
                    // Password validated

                    //This vec exists because I need to set the
                    //status code BEFORE starting the response
                    //
                    //Now all my functions think they are writing
                    //to the response but they're just writing to
                    //this vec
                    let mut res_started: Vec<u8> = Vec::new();
                    let error = match util::parse_uri(&req.uri.clone()) {
                        Ok(uri) => match uri.as_slice() {
                            &["block",block_num] => self.get_block(block_num, &mut res_started),
                            &["block",block_num,action_num] => self.get_action(block_num,action_num, &mut res_started),
                            &["actions"] => {
                                self.get_action_queue(&mut res_started)
                            }
                            &["currentblock"] => {
                                self.get_current_block_number(&mut res_started)
                            }
                            &["action"] => {
                                self.submit_action(&mut req, &mut res_started)
                            }
                            &["block"] => {
                                self.submit_block(&mut req, &mut res_started)
                            }
                                _ => Err(HandlerError::InvalidUri)
                            },
                        Err(e) => Err(e)
                        };
                    match error {
                        //Request successfully handled
                        Ok(_) => {
                            *res.status_mut() = StatusCode::Ok;
                            //Copy dummy response into response
                            //Kills this thread if client hung up
                            io::copy(&mut res_started.as_slice(),&mut res.start().unwrap()).unwrap();
                        },
                        //Print and return error
                        Err(e) => {
                            *res.status_mut() = StatusCode::InternalServerError;
                            match e {
                                HandlerError::BlockError(BlockError::SwitchChain(b)) => match self.switch_chains(b.get_number()) {
                                    Ok(_) => {
                                        //Resubmit block after switching chains
                                        //Create clean buffer
                                        let mut buffer: Vec<u8> = Vec::new();
                                        match self.submit_downloaded_block(b,&mut buffer) {
                                            Ok(_) => {
                                                *res.status_mut() = StatusCode::Ok;
                                                //Copy dummy response into response
                                                //Kills this thread if client hung up
                                                io::copy(&mut buffer.as_slice(),&mut res.start().unwrap()).unwrap();
                                            },
                                            Err(err) => {
                                                println!("Error handling request: {:?}",err);
                                                //Kill thread if writing fails
                                                res.start().unwrap().write_all(format!("Error handling request: {:?}",err).as_bytes()).unwrap();
                                            }
                                        }
                                    },
                                    Err(err) => {
                                        println!("Error handling request: {:?}",err);
                                        //Kill thread if writing fails
                                        res.start().unwrap().write_all(format!("Error handling request: {:?}",err).as_bytes()).unwrap();
                                    }

                                },

                                _ => {
                                    println!("Error handling request: {:?}",e);
                                    //Kill thread if writing fails
                                    res.start().unwrap().write_all(format!("Error handling request: {:?}",e).as_bytes()).unwrap();
                                }
                            }

                        }
                    }
                } else {
                    *res.status_mut() = StatusCode::Forbidden;
                    res.send(b"Access denied").unwrap();
                }
            }
            hyper::Get => {
                // Match URI instead of function header?
                // Should I just always match the URI?
                // TODO implement this
                io::copy(&mut req, &mut res.start().unwrap()).unwrap();
            }
            _ => *res.status_mut() = StatusCode::MethodNotAllowed,
            // Written when dropped
        }
    }
}
#[cfg(test)]
use std::path::PathBuf;
#[test]
fn get_current_block_test() {
    let handler = Handler::new(config::Config::new(PathBuf::new()),42);
    let mut writable: Vec<u8> = vec![];
    let mut final_string = String::new();
    handler.get_current_block_number(&mut writable).unwrap();
    //'4','2' is two bytes
    assert_eq!((&writable as &[u8]).read_to_string(&mut final_string).unwrap(),2); 
    assert_eq!(final_string,"42")
}
